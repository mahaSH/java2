
package day04namesdynamic;

import java.util.ArrayList;
import java.util.Scanner;


public class Day04NamesDynamic {

    
    public static void main(String[] args) {
       Scanner input=new Scanner(System.in);
        int num;
       
        
        System.out.print("How many names you want to enter?");
        num=input.nextInt();
        input.nextLine();//consume the left over newline
        ArrayList<String> nameList=new ArrayList<>();      
        
        for (int i = 0; i < num; i++) {
            System.out.printf("Enter name %d#:", i+1 );    
            String name=input.nextLine();
            nameList.add(name);
            
            
        }
        System.out.print("your names were:");
        for (int i = 0; i < nameList.size(); i++) {
            System.out.printf("%s%s",i==0 ? "": ",", nameList.get(i));
        }
    }
    
}
