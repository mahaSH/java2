/**Day04Numbers
+------------
+
+Ask user how many numbers he/she wants to generate.
+Generate the numbers as random integers in -100 to +100 range, both inclusive.
+Place the numbers in ArrayList<Integer> named numsList.
+
+In the next loop find the numbers that are less or equal to 0 and print them out, one per line.
+
+*/
package day04numbers;

import java.util.*;

public class Day04Numbers {

    
    public static void main(String[] args) {
     Scanner input=new Scanner(System.in);   
     ArrayList<Integer> numsList=new ArrayList<>();
     Random random=new Random();
     int result;
     int number;
    System.out.println("How many numbers do you want to generate?");
    number=input.nextInt();

    for(int i=0;i<number;++i){
    result = random.nextInt(201)-100;
    numsList.add(result);
    }//End of for loop

    for (int i = 0; i < number; i++) {
     if(numsList.get(i)<=0) {                    
        System.out.println(numsList.get(i));   
     }//End of if statement
    }//End of for loop
       
}//End of main method
}//End of the class


