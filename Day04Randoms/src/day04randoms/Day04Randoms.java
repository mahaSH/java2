
package day04randoms;
import java.util.Random;
import java.util.Scanner;       
 
public class Day04Randoms {

    
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int maximum;
        int minimum;
        Random random=new Random();
        int result;
        
        System.out.println("Enter the  minimum number");
        minimum=input.nextInt();
        System.out.println("Enter the  maximum number");
        maximum=input.nextInt();
         if(minimum>maximum||minimum<0||maximum<0){
             System.out.println("Error.minimum must be less than or equal to maximum and both should be positive!!");
             System.exit(1);
             }//End of if statement
         for(int counter=0;counter<10;++counter){
             result = random.nextInt(maximum-minimum+1) + minimum;
             System.out.println(result);
            
             }//End counter
         }
        
        
        
    }
    
