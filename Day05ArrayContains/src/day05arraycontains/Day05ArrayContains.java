/** Part 1:

Create method with the following signature:

public int[] concatenate(int[] a1, int[] a2) { }

Return an array that is a concatenation of the two arrays passed.
Example: if I pass { 2, 7, 8} and { -2, 9} I will receive back array
{ 2, 7, 8, -2, 9 }


* Part 2:

Write a method with the following signature:

public static void printDups(int[] a1, int a2[]) { }

That method will print out only values that appear both in a1 and a2.
If a1 contains multiple values that are the same you may print them out multiple times.

For arrays:
a1: 1, 3, 7, 8, 2, 7, 9, 11
a2: 3, 8, 7, 5, 13, 5, 12
Output is:
3
7
8
7


* Part 3

Create method with the following signature:

public int[] removeDups(int[] a1, int[] a2) { }

Return a copy of a1, except elements that are also present in a2 are removed.
Note: returned array must be of exactly the needed size, not larger.

Example: removeDups({2, 3, 7, 9, 3 }, {3, 7}) will return array of two elements exactly: {2, 9}


NOTE: For all methods above, it is up to you to come up with main() method content to test your solutions.

NOTE: You are NOT allowed to use ArrayList or another dynamic structure to solve the above task. Only arrays.


 */
package day05arraycontains;

/**
 *
 * @author TJ
 */
public class Day05ArrayContains {

   public static int[] concatenate(int[] a1, int[] a2) { 
    //Return an array that is a concatenation of the two arrays passed.Example: if I pass { 2, 7, 8} and { -2, 9} I will receive back array{ 2, 7, 8, -2, 9 }   
     
    int a1Length,a2Length;
    a1Length=a1.length;
    a2Length=a2.length;
    int k=0;
   int[] concatenated=new int[a1Length+a2Length];
       for (int i = 0; i < a1Length; i++) {
         concatenated[i]=a1[i];
         
       }
       for (int i = 0; i < a1Length; i++) {
         concatenated[a1Length+i]=a2[i];
         
       }
    
    return concatenated;
   }
   /* Part 2:

Write a method with the following signature:

public static void printDups(int[] a1, int a2[]) { }

That method will print out only values that appear both in a1 and a2.
If a1 contains multiple values that are the same you may print them out multiple times.

For arrays:
a1: 1, 3, 7, 8, 2, 7, 9, 11
a2: 3, 8, 7, 5, 13, 5, 12
Output is:
3
7
8
7*/
public static void printDups(int[] a1, int a2[]) {
    int counter1=0;
    int counter2=0;     
    for(int i = 0; i < a1.length; i++){
    for(int j = 0; j < a2.length; j++){
        if(a1[i] == a2[j]){
          //dups[counter1]=a1[i];
           counter1++;    
        }
    }
    }
    int dups[]=new int[counter1];
    
    for(int k = 0; k < a1.length; k++){
    for(int j = 0; j < a2.length; j++){
        if(a1[k] == a2[j])
        {
          dups[counter2]=a1[k];
           counter2++ ; 
        }
    }  
    }
    
    for (int j = 0; j < counter1; j++) {
        System.out.printf("%s%d",j==0?"":",",dups[j]);
    }
     
}
/** Part 3

Create method with the following signature:

public int[] removeDups(int[] a1, int[] a2) { }

Return a copy of a1, except elements that are also present in a2 are removed.
Note: returned array must be of exactly the needed size, not larger.

Example: removeDups({2, 3, 7, 9, 3 }, {3, 7}) will return array of two elements exactly: {2, 9}*/
   public static int[] removeDups(int[] a1, int[] a2) {
       int dups=0;
       int k=0;  
       for(int i = 0; i < a1.length; i++){
       for(int j = 0; j < a2.length; j++){
        if(a1[i] == a2[j]){
           dups+=1;            
        }
    }
}
       int[]duplication=new int[(a1.length)-dups];
        for(int i = 0; i < a1.length; i++){
        for(int j = 0; j < a2.length; j++){
        if(a1[i] != a2[j]){
        duplication[k]=a1[i];
           k++; 
        }
    }
}
    return duplication;   
   }  
    public static void main(String[] args) {
        int[]a1={2,5,7,3};
         int[]a2={2,0,7,4};
         int[]a3;
         int[]a4;
         printDups( a1, a2);
         a3=removeDups(a1,a2);
         a4=concatenate(a1, a2);
         for (int i = 0; i < a3.length; i++) {
            
       
        System.out.println(a3[i]);
         }
         System.out.println("\n"+"------------------");
          for (int i = 0; i < a4.length; i++) {
        System.out.println(a4[i]);    
     }
    
        
        
    }
    
}
