package day05gpaconversion;

import java.io.InputStreamReader;

import java.net.URL;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections; 

/**
 *
 * @author Maha
 */
public class Day05GPAConversion {
private ArrayList<String> arrayList;     
static double gpaToNum(String gpaStr){
    switch(gpaStr){
        case"A":
            return 4.0;
        case"A-":
            return 3.7;
        case"B+":
            return 3.3;
        case"B":
            return 3.0;
        case"B-":
            return 2.7;
        case"C+":
            return 2.3;
        case"C":
            return 2;
        case"D":
            return 1.0;
        case"F":
            return 0;   
        default:
           return -1;
                
            
    }
}


   
    public static void main(String[] args) 
             
    {
        double average,median;
        double sum=0.0;
        int mid;
        ArrayList<Double>grades=new ArrayList<>();
    try(Scanner fileInput=new Scanner (new File("file.txt"))){
        while (fileInput.hasNextLine()){
            String line=fileInput.nextLine();
            double gpaVal=gpaToNum(line);
           
            
            
            if(gpaVal==-1){
                System.out.println("Error parsing value,skipping:"+line);
                continue;
            }
            grades.add(gpaVal);
            for (int i = 0; i < grades.size(); i++) {
                System.out.printf("%s %s", i==0 ? "":";","grades.get(i)");
                sum+=grades.get(i);
                
            }
            
            average=sum/grades.size();
            Collections.sort(grades) ;
            if(grades.size()%2==0){
                mid=(grades.size()/2);
                median=(grades.get(mid+1)*grades.get(mid-1))/2;
            }
            else {
                median=grades.get(grades.size()/2);
            }
            }
    }
    

 catch (IOException ex) {
   System.out.println("Error reading file:"+ex.getMessage());
          
        }
            
     }
    
}
