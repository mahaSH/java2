/*
 Declare:

ArrayList<double> numList = new ArrayList<>();

In a loop ask user for numbers and put them into the list.
Entering 0 ends the input / exits the loop.

In another loop or loops compute and display (with 2 decimal points precision):
* sum of all numbers
* average of all numbers
* maximum
* minimum

Example session:
Enter a postive number: 12.20
Enter a postive number: 1.8
Enter a postive number: 10
Enter a postive number: 0

Sum of all numbers is: 24.00
Average of all numbers is: 8.00
Maximum: 12.20
Minimum: 1.80

 */
package day05maxmin;

import java.util.ArrayList;
import java.util.*;


public class Day05MaxMin {

   static Scanner input=new Scanner(System.in);
    public static void main(String[] args) {
        Double number,sum,average,maximum,minimum;
        
        // TODO code application logic here
        ArrayList<Double> numList = new ArrayList<>();
         // In a loop ask user for numbers and put them into the list.Entering 0 ends the input / exits the loop.
        
         while(true){
             System.out.print("Enter a postive number: ");
             number=input.nextDouble();
             if(number.compareTo(0.0)==0){
                 
                 break;
                }
             else if(number.compareTo(0.0)<0){
                 System.out.println("negative number is entered.The program is termenated!!");
                 break;
             }
             else{
                 numList.add(number);
             }
             }
         /*In another loop or loops compute and display (with 2 decimal points precision):
          * sum of all numbers
          * average of all numbers
          * maximum
          * minimum*/
         sum=0.0;
         for (int i = 0; i <numList.size(); i++) {
             sum+=numList.get(i);
                      
        }
         
         average=sum/numList.size();
         System.out.printf("%nSum of all numbers is: %.2f %nAverage of all numbers is: %.2f ",sum,average);
        maximum=numList.get(0);
        minimum=numList.get(0);
        for (int i = 0; i < numList.size(); i++) {
            if(numList.get(i)>maximum){
                maximum=numList.get(i);
            }
            if(numList.get(i)<minimum){
                minimum=numList.get(i);
            }
          
        }
        System.out.printf("%nmaximum is: %.2f %nminmum is: %.2f ",maximum,minimum);  
         }
    }
    

