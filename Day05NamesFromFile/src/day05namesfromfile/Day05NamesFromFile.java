/*In the main directory of the project create "names.txt" file with serveral names in it, one per line.

Read the file, line by line and place each name in 
ArrayList<String> nameArray = new ArrayList<>();

Display all names back to the user, comma-separated on one line.

Ask user for a search string and save it in String search variable;

String search = ...

In another loop find all names that contain the search string and display them.
Also write each of those names to "found.txt" file, one name per line.

In another loop find the longest name and display it.

Writhe the longest name to file "longestname.txt"

Example session:

Names read from file:
Jerry, Malexandra, Trixie, Tom, Barry

Enter serach string: rr

Matching name: Jerry
Matching name: Barry

Longest name is: Malexandra


Preferred solution does NOT use sorting.
*/
package day05namesfromfile;

import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Maha M.Shawkat
 */
public class Day05NamesFromFile {

    static Scanner input=new Scanner(System.in);
    public static void main(String[] args) {
        ArrayList<String> names=new ArrayList<>();
        String search;
        String longestStr;
        
       
        
       try(Scanner fileInput=new Scanner(new File("names.txt"))){
        while (fileInput.hasNextLine()) {
            String outline=fileInput.nextLine();
            names.add(outline);
             
        } 
        System.out.println("Names read from file:"); 
           for (int i = 0; i < names.size(); i++) {
               System.out.printf("%s%s",i==0 ?" ":",",names.get(i));
           }
           System.out.println("\n Enter serach string:  \n");
           Scanner input=new Scanner(System.in);
           search=input.nextLine();
         //In another loop find all names that contain the search string and display them.  
           System.out.println("The names that contain yor search string is(are):");  
           PrintWriter outFile=new PrintWriter(new File("found.txt"));
         for (int i = 0; i < names.size(); i++) {
            if(names.get(i).contains(search)){
                System.out.println(" Matching name: "+names.get(i));
                //write each of those names to "found.txt" file, one name per line.
                outFile.println(names.get(i));
            }
         }  
                 outFile.close();
                     
   // In another loop find the longest name and display it.

          PrintWriter outfile2=new PrintWriter(new File("longestname.txt"));
         longestStr=names.get(0);
         for (int i = 0; i <names.size() ; i++) {
             if(names.get(i).length() > longestStr.length()){
                 longestStr=names.get(i);
                 //Writhe the longest name to file "longestname.txt"
               
             }
               
           }
           System.out.println("\n Longest name is :\n"+longestStr);
          outfile2.println(longestStr);
         outfile2.close();
       }catch(IOException ex){
               System.out.println("Error reading file:"+ex.getMessage());
               }
    }
       
    
}
