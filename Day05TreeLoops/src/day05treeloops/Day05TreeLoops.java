
package day05treeloops;

import java.util.Scanner;


public class Day05TreeLoops {
static Scanner input=new Scanner(System.in);
    public static void main(String[] args) {
        int number;
        int count=1;
        System.out.println("How many lines you want to enter?");
        number=input.nextInt();
        input.nextLine();
         while(number!=0) {
             for (int i = 0; i < number-1; i++) {
                 System.out.printf(" "); 
             }
                 for (int j = 0; j < count; j++) {
                     System.out.print("*");
                 }
                 count+=2;
                 System.out.println(" "); 
             
            
            
         number--;   
        }
    }
    
}
