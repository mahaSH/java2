
package day06peopledualarray;

import java.util.Scanner;

public class Day06PeopleDualArray {


    public static void main(String[] args) {
       String[] namesArray = new String[4];
        int[] agesArray = new int[4];
        
        Scanner input=new Scanner(System.in)  ;
        for (int i = 0; i <4; i++) {
            System.out.print("Enter name of person #"+(i+1+": "));
            namesArray[i]=input.nextLine();
            System.out.print("Enter age of person #"+(i+1+": "));
            agesArray[i]=input.nextInt();
            input.nextLine();
        }
        int youngestIndex=0;
        for (int i = 0; i < 4; i++) {
            if(agesArray[i]<youngestIndex) {
              youngestIndex=i  ;
            }          
        }
        System.out.printf("Youngest person is %d and the name is %s",agesArray[youngestIndex],namesArray[youngestIndex]);
    }
    
}
