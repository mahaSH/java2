
package day08peopleagain;

import java.util.ArrayList;

 class Person{

    public Person(String name, int age) {
       setName(name) ;
       setAge(age);
    }
     
     private String name;//length 2-50
     private int age;//1-150

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length()<2||name.length()>50){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<1||age>50){
            throw new IllegalArgumentException("Name must be between 1-50 characters long");
        }
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format(name, age);
                //String.format"Person{" + "name=" + name + ", age=" + age + '}';
    }
  
 
 }
class Student extends Person{

    //Add constructor taking four parameters:name,age subject;gpa
    public Student(String name, int age,String program, double gpa ) {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }
    
    //Encapsulate fields and add verevications in setters
    
    private String program;
    private double gpa;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if(program.length()<2||program.length()>50){
        throw new IllegalArgumentException("Program name must be between 1-50 characters long");
        }
        this.program = program;
    }

    public double getGpa() {
        
        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa<0.0||gpa>4.0){
            throw new IllegalArgumentException("gpa must be between 0.0 and 4.0 characters long");
        }
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        
        return String.format("Student name is%s with age aof%dhe is studying%sand his grade is%.2f ",getName,getAge,program,gpa);
    }

    
    
    
}
class Teacher extends Person{
    //Add constructor taking four parameters:name,age subject;gpa
    
    //Encapsulate fields and add verevications in setters
    private String subject;
    private int YearsOfExperience;

    public Teacher(String name, int age,String subject, int YearsOfExperience) {
        super(name, age);
        setSubject(subject);
        setYearsOfExperience( YearsOfExperience);
        
    }

    public String getSubject() {
        
        return subject;
    }

    public void setSubject(String subject) {
        if(subject.length()<2||subject.length()>50){
        throw new IllegalArgumentException("subject name must be between 1-50 characters long");
        }
        this.subject = subject;
    }

    public int getYearsOfExperience() {
        return YearsOfExperience;
    }

    public void setYearsOfExperience(int YearsOfExperience) {
        if (YearsOfExperience<0||YearsOfExperience>100){
            throw new IllegalArgumentException("Years Of Experience must be between 1-50 characters long");
        }
        this.YearsOfExperience = YearsOfExperience;
    }

    @Override
    public String toString() {
        
        return String.format("Teacher{" + "subject=" + subject + ", YearsOfExperience=" + YearsOfExperience + '}');
    }

    
    
}
public class Day08PeopleAgain {
   static ArrayList<Person> peopleList = new ArrayList<>();
    
    public static void main(String[] args) {
     
           
                //Create two objects of each type(6 in total)ad add the to peopleList
                Person p1=new Person("alice",22);
                peopleList.add(p1);
                Person p2=new Person("bob",33);
                 peopleList.add(p2);
                 Student s1=new Student("merry",22,"java",3.0);
                peopleList.add(s1);
                Student s2=new Student("carry",33,"java",2.0);
                peopleList.add(s2);
                Teacher t1=new Teacher("jean",55,"oracle",30);
                peopleList.add(t1);
                Teacher t2=new Teacher("vean",55,"programming 1",30);
                peopleList.add(t2);
                
                //In aloop printout all the information of the objects one per line
                for (Person p:peopleList){
                    System.out.println(p);//polymorphic call to toString
            
        }
                //suggestion-you may use 'instanceof"
                //In a lop printou informations only for the students from peopleList
       
    }
    
}
