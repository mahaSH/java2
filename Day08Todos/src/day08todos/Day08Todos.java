package day08todos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

enum TaskStatus { Pending, Done };

class InvalidDataException extends Exception {

    public InvalidDataException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
    public InvalidDataException(String msg) {
        super(msg);
    }
}

class Todo {

    public Todo(String task, Date dueDate, int hoursOfWork,TaskStatus status) throws InvalidDataException {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
        SetTaskStatus(status);
        
    }

    public Todo(String dataLine) throws InvalidDataException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new InvalidDataException("Error: invalid number of items in line: " + dataLine);
            }
            setTask(data[0]); // IllegalArgumentException
            setDueDate(dateFormatFile.parse(data[1])); // ParseException
            setHoursOfWork(Integer.parseInt(data[2])); // NumberFormatException
            SetTaskStatus(status.valueOf(data[3]));
        } catch (IllegalArgumentException | ParseException ex) {
            // exception chaining
            throw new InvalidDataException("Error: parsing failed", ex);
        }
    }


    
    
    private String task; // 2-50 characters long, must NOT contain a semicolon or | or ` (reverse single quote) characters
    private Date dueDate; // Date between year 1900 and 2100
    private int hoursOfWork; // 0 or greater number
    private TaskStatus status;

    // format all fields of this Todo item for display exactly as specified below in the example interactions
    @Override
    public String toString() {
        String dueDateStr = dateFormatScreen.format(dueDate);
        return String.format("%s, %s, will take %d hour(s) of work.the status is %s", task, dueDateStr, hoursOfWork,status);
    }

    public String toDataString() {
        String dueDateStr = dateFormatFile.format(dueDate);
        return String.format("%s;%s;%d", task, dueDateStr, hoursOfWork);
    }
    public TaskStatus getTaskStatus(){
        return(status);
    }
    public String getTask() {
        return task;
    }
  public void SetTaskStatus(TaskStatus status){
      this.status=status;
  }

    public void setTask(String task) throws InvalidDataException {
        if (!task.matches("[^|`;]*")) {
            throw new InvalidDataException("Task must not contain ; ` | characters");
        }
        if (task.length() < 2 || task.length() > 50) {
            throw new InvalidDataException("Task must be between 2-50 chars long");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) throws InvalidDataException {
        // TODO: find a way to check year, suggestion use Calendar class
        //int year = dueDate.getYear();
        //if (year < 0 || year > 200) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 | year > 2100) {
            throw new InvalidDataException("Year must be between 1900 and 2100");
        }
        this.dueDate = dueDate;
    }

    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) throws InvalidDataException {
        if (hoursOfWork < 0) {
            throw new InvalidDataException("Hours of work can't be less than 0");
        }
        this.hoursOfWork = hoursOfWork;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    
    final static SimpleDateFormat dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
    final static SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");
}

public class Day08Todos {

    static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is: ");
        
            int choice = input.nextInt();
       try{ input.nextLine();
        return choice;
        }//end try
        catch(InputMismatchException ex){
            System.out.println("Invalid input, please try again");
            input.nextLine();
        }//end catch
        return choice;
    }

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        loadDataFromFile();
        
            while (true) {
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addTodo();
                    break;
                case 2:
                    listAllTodos();
                    break;
                case 3:
                    deleteTodo();
                    break;
                case 4:
                    modifyTodo();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting. Good bye!");
                    return;
                default:
                    System.out.println("Invalid choice, try again.");
            }
            System.out.println();
        }
    }

    private static void addTodo() {
        try {
            System.out.println("Adding a todo.");
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter hours of work (integer): ");
            int hours = input.nextInt();
            input.nextLine();
            TaskStatus status=TaskStatus.Pending;
            Todo todo = new Todo(task, dueDate, hours,status);
            todoList.add(todo);
            System.out.println("You've created the following todo:");
            System.out.println(todo);
        } catch (ParseException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        } catch (IllegalArgumentException | InvalidDataException ex) {
            System.out.println("Data error: " + ex.getMessage());
        }
    }

    private static void listAllTodos() {
        if (todoList.isEmpty()) {
            System.out.println("There are no todos.");
            return;
        }
        for (int i = 0; i < todoList.size(); i++) {
            System.out.printf("#%d: %s\n", i + 1, todoList.get(i));
        }
    }

    private static void deleteTodo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void modifyTodo() 
        {
    System.out.printf("Which todo you want to modify?");
    int lineToModify=input.nextInt();
    if(lineToModify>todoList.size()){
        System.out.println("Not excisted list number");
    }
    try{
        input.nextLine();
    
    System.out.print("Enter the new task description: ");
            String task = input.nextLine();
            System.out.print("Enter the new due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter the new hours of work (integer): ");
            int hours = input.nextInt();
            input.nextLine();
            System.out.print("Enter task status: Pending or Done");
            if(!input.nextLine().equals("Done")&&!input.nextLine().equals("Pending")){
                System.out.println("Bad status entered..exiting");
            }
            TaskStatus status=TaskStatus.valueOf(input.nextLine());
            Todo todo = new Todo(task, dueDate, hours,status);
            todoList.set(lineToModify,todo);
            System.out.println("You've modified task #"+lineToModify+" to the following todo:");
            System.out.println(todo);
    }//try end
            
                catch(ParseException|InvalidDataException| IllegalArgumentException ex){
                        System.out.println("parsing error "+ex.getMessage());
                        }//Catch end
            }
    

    final static String FILE_NAME = "todos.txt";

    static void loadDataFromFile() {
        try (Scanner inputFile = new Scanner(new File(FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                try {
                    String line = inputFile.nextLine();
                    Todo todo = new Todo(line);
                    todoList.add(todo);
                } catch (InvalidDataException ex) {
                    // ex.printStackTrace();
                    System.out.println("Error parsing line, skipping: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    static void saveDataToFile() {
        try (PrintWriter fileOutput = new PrintWriter(new File("x" + FILE_NAME))) {
            for (Todo todo : todoList) {
                fileOutput.println(todo.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing file: " + ex.getMessage());
        }
    }

}

/*package day08todos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

class Todo {
	private String task; 
        private Date dueDate; 
	private int hoursOfWork; 

    public Todo(String task, Date dueDate, int hoursOfWork) {
        setTask(task);
        setDueDate(dueDate);
        setHoursOfWork(hoursOfWork);
    }
    //This constructor will split and parse line's content and assign values to fields using setters.
    //In case a problem is encountered it will throw one or more exceptions to signal a failure to create an object.
    public Todo(String dataLine) {
        try{
        String[] data=dataLine.split(";");
        setTask(data[0]);
        SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd"); 
        setDueDate(date.parse(data[1]));
        setHoursOfWork(Integer.parseInt(data[2]));
    }//End of try block
        catch (ParseException e) {
            e.printStackTrace();
        }//End of catch
    }//End constructor2
    
//That method is similar to toString() however instead of providing String formatted for user's viewing pleasure it provides a string formatted in exactly the same way as each line in the data file. Example:
//Buy milk 2%;2019-11-22;2
    public String toDataString() {
     return String.format("%s;%s;will take %d",getTask(),getDueDate(),getHoursOfWork());   
    }//End toDataString method



    public int getHoursOfWork() {
        return hoursOfWork;
    }

    public void setHoursOfWork(int hoursOfWork) {
        // 0 or greater number
        if(hoursOfWork<0){
          throw new IllegalArgumentException ("hours should be greater than zero ");  
        }
        this.hoursOfWork = hoursOfWork;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        if(!task.matches("[^|`;]*")){
          throw new IllegalArgumentE  
        }//
        // 2-50 characters long
        
        if(task.length()<2||task.length()>50||task.contains(";")||task.contains("|")||task.contains("`")){
            throw new IllegalArgumentException ("task name should be 2-50 characters ");
        }
        this.task = task;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        // Date between year 1900 and 2100
        if(dueDate.getYear()<1900 ||dueDate.getYear()>2100){
            throw new IllegalArgumentException ("due date should be between year 1900 and 2100");  
        }
        this.dueDate = dueDate;
    }
   @Override
	public String toString() {
        return String.format("The task is %s the dueDate is %s the number of hours to work is %d",getTask(),getDueDate(),getHoursOfWork());
        }  

    static class dateFormatScreen {

        public dateFormatScreen() {
        }
    }
}

public class Day08Todos {
static ArrayList<Todo> todoList = new ArrayList<>();
static Scanner input = new Scanner(System.in);
static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Add a todo\n"
                + "2. List all todos (numbered)\n"
                + "3. Delete a todo\n"
                + "4. Modify a todo\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = input.nextInt();
        input.nextLine();
        return choice;
    }

//The following methd will load data from "todos.txt" file and instantiate one object for every line read, then add it to todoList.
//To parse lines you should use the 2nd constructor described above, one that takes an entire line as its only parameter.
//??????Note that this constructor may throw one or more different exceptions if a problem with line's content is encountered.
//?????In case an invalid line is encountered the program should inform the user and continue to the next line.
//the program needs to call this method when it starts, before menu is displayed.
static void loadDataFromFile() {
    try{
    Scanner fileInput=new Scanner(new File("todos.txt"));
    Todo t;
    while(fileInput.hasNextLine()){
      String outLine=fileInput.nextLine();
      Todo toDo=new Todo(outLine);
    }//End of while loop
     
    }//End of try1
    catch(FileNotFoundException ex){
        System.out.println("Error reading file"+ex.getMessage()); 
    }//End catch1
   
}//End of loadDataFromFile()


//The following method will save data from todoList back to "todos.txt" file using toDataString() method on each object on the list.
//In case an issue is encountered the program should inform the user and go back to the menu.
//Your program should call this method right before it exits.
static void saveDataToFile() {
    try{
        PrintWriter writer=new PrintWriter(new File("todos.txt"));
    }//End of try block
    catch(FileNotFoundException ex){
        System.out.println("Error writing to file"+ex.getMessage());
    }//End of catch block
}//End of saveDataToFile() 
//list to do
static void listAllTodos(){
    System.out.println("Listing all todos.");
    for (int i = 0; i < todoList.size(); i++) {
        if(todoList.size()<=0){
            System.out.println("There are no todos.");
        }//End of if statement
        System.out.printf("%d",i+1,"-"); 
        System.out.println(todoList.get(i));    
        }//End of for loop
}//end of ListAllTodos method
static void deleteTodo(){
   
    System.out.println("Deleting a todo. Which todo # would you like to delete?");
    int delete=input.nextInt();
    todoList.remove(delete+1);
    System.out.printf("Deleted todo #%d successfully.",(delete+1));
    
}//End of DeleteAtodo method
//Modify a todo
static void modifyTodo(){
    System.out.printf("Which todo you want to modify?");
    int modify=input.nextInt();
    input.nextLine();
    System.out.print("Enter the new task description: ");
            String task = input.nextLine();
            System.out.print("Enter the new due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter the new hours of work (integer): ");
            int hours = input.nextInt();
            Todo todo = new Todo(task, dueDate, hours);
            todoList.get(modify)=todo;
            System.out.println("You've modified task #"+modify+" to the following todo:");
            System.out.println(todo);
   
}//End of ModifyATodo method


    
    public static void main(String[] args) {
        loadDataFromFile();
        while (true) {
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addTodo();
                    break;
                case 2:
                    listAllTodos();
                    break;
                case 3:
                    deleteTodo();
                    break;
                case 4:
                    modifyTodo();
                    break;
                case 0:
                    System.out.println("Exiting. Good bye!");                    
                    return;
                default:
                    System.out.println("Invalid choice, try again.");
            }
            System.out.println();
            saveDataToFile();
        }
        
    }

    private static void addTodo() {
        try {
            System.out.println("Adding a todo.");
            System.out.print("Enter task description: ");
            String task = input.nextLine();
            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dueDateStr = input.nextLine();
            Date dueDate = Todo.dateFormatScreen.parse(dueDateStr);
            System.out.print("Enter hours of work (integer): ");
            int hours = input.nextInt();
            Todo todo = new Todo(task, dueDate, hours);
            todoList.add(todo);
            System.out.println("You've created the following todo:");
            System.out.println(todo);
        }
        catch (ParseE) ex) {
            System.out.println("Not supported yet.");    

        }
        
    }

   
}*/