package day09airporttravel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

class ParameterInvalidException extends RuntimeException {

    public ParameterInvalidException(String message) {
        super(message);
    }
}

class Airport {

    String code, city;
    double latitude, longitude;

    //Constructors,Setters and getters
    public Airport() {
    }

    public Airport(String code, String city, double latitude, double longitude) {
        setCode(code);
        setCity(city);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public Airport(String line) throws ParameterInvalidException {
        String[] data = line.split(";");
        try {
            if (data.length != 4) {
                throw new ParameterInvalidException("Bad information entered");
            }//end line check
            setCode(data[0]);
            setCity(data[1]);
            setLatitude(Double.parseDouble(data[2]));
            setLongitude(Double.parseDouble(data[3]));
        }//End try block
        catch (ParameterInvalidException ex) {
            throw new ParameterInvalidException("Parsing error");
        }//ed catch block   
    }//end constructor

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        if (!code.matches("[A-Z]{3}")) {
            throw new ParameterInvalidException("Airport code shold be 3 upper case letters only");
        }//End regex checking
        this.code = code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if (city.isEmpty() || city.matches("[\\;]")) {
            throw new ParameterInvalidException("city should  not be empty string and no semicolon is allowed");
        }//End regex checking
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) throws ParameterInvalidException {
        try{if (latitude < 0 || latitude > 90) {
            throw new ParameterInvalidException("Latitude range from 0 to 90.0");
        }//End of latitude check  
        }
        catch(ParameterInvalidException ex){
        throw new ParameterInvalidException("Latitude range from 0 to 90.0");
    }
        this.latitude = latitude;
        
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) throws ParameterInvalidException {

        if (longitude < 0.0 || longitude > 180.0) {
            throw new ParameterInvalidException("Longitude rang is from 0 to 180");
        }//End of logitude check    
        this.longitude = longitude;

    }

    @Override
    public String toString() {
        return String.format("Airport code:%s,City name:%s,Latitude:%f;,longitude:%f" ,getCode() ,getCity(),getLatitude(),getLongitude());
    }
    
}//WEnd airPort class

public class Day09AirportTravel {

    static ArrayList<Airport> airportList = new ArrayList<>();
    static Scanner input=new Scanner(System.in);
//Static methods
    static int inputInt(){
        while(true){
          try{
              int value=input.nextInt();
          
          input.nextLine();
          return value;
          }//End try
          catch(InputMismatchException ex){
              System.out.println("Invalid integer,please retry again");  ;
              input.nextLine();
          }//End catch
        }//End while
    }//End method
    //Static methods
    static double inputFloat(){
        while(true){
          try{
              double value=input.nextDouble();
          
          input.nextLine();
          return value;
          }//End try
          catch(InputMismatchException ex){
              System.out.println("Invalid integer,please retry again");  ;
              input.nextLine();
          }//End catch
        }//End while
    }//End method
    static public void readFromFile(){
        try {
            Scanner inputFile = new Scanner(new File("airports.txt"));

            while (inputFile.hasNextLine()) {

                Airport airport = new Airport(inputFile.nextLine());
                airportList.add(airport);

            }//end of while loop

        }//End of try method
        catch (FileNotFoundException ex) {
            throw new ParameterInvalidException("No file to be read");
        }//End catch
    }//End readFromFile
    static public void writeToFile(Airport airPort){
        try{
            PrintWriter print=new PrintWriter(new File("airports.txt"));
            print.println(airPort.getCode()+";"+airPort.getCity()+";"+Double.toString(airPort.getLatitude())+";"+Double.toString(airPort.getLongitude()));
    }//end of Tryblock
        catch( FileNotFoundException ex){
            throw new ParameterInvalidException("IO error while  writing  on the file");
        }//end try
    }//end writeToFile
    //Show all airports (codes and city names) method
    static public void showAll(){
        for (int i = 0; i < airportList.size(); i++) {
            System.out.printf("%d-",(i+1)); 
            System.out.println(airportList.get(i));
        }//End for loop
        System.out.println("End of all the list");
    }//end of show all method
    //Find distance between two airports by codes.
    static public double distance(Airport airport1,Airport airport2){ 
    final int R = 6371; // Radius of the earth
    double latDistance = Math.toRadians(airport2.getLatitude() - airport1.getLatitude());
    double lonDistance = Math.toRadians(airport2.getLongitude() - airport1.getLongitude());
    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(airport1.getLatitude())) * Math.cos(Math.toRadians(airport2.getLatitude()))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double distance = R * c * 1000; // convert to meters
    return distance;
    }//end distance method
    // Find the 1 airport nearest to an airport given and display the distance.
    static public Airport nearest(Airport airport){
        Airport nearest=airportList.get(0);
        for (int i = 0; i < airportList.size(); i++) {
         if(distance (airportList.get(i),airport)<distance (nearest,airport)&&distance (airportList.get(i),airport)!=0){
           nearest=airportList.get(i);  
         }//End if staement
        }//End for statement
        
      return nearest;
       }//End of nearest method
     //Add a new airport to the list.
     static public void addToAirportList(){
         System.out.print("Enter the airport code ");
         String code=input.nextLine();
         System.out.print("Enter the city name ");
         String city=input.nextLine();
         System.out.print("Enter the latitude ");
         double lat=inputFloat();
         System.out.print("Enter the longitude ");;
         double lon=inputFloat();
         Airport airport=new Airport(code,city,lat,lon);
         airportList.add(airport);
     }//end addToAirportList method 
   
public static void main(String[] args) {
        //readFromFile();
        
        while(true){
            
        
        System.out.println("1. Show all airports (codes and city names)\n" +
"2. Find distance between two airports by codes.\n" +
"3. Find the 1 airport nearest to an airport given and display the distance.\n" +
"4. Add a new airport to the list.\n" +
"0. Exit.\n"+"Pelase enter your choise \n");
        try{
            int choise=inputInt();
        switch(choise){
            case 1:
                showAll();
                break;
            case 2:
                System.out.print("Enter the airports numbers:"
                        );
                int airport1=inputInt();
                int airport2=inputInt();
                distance(airportList.get(airport1),airportList.get(airport2));
                break;
                case 3:
                    System.out.println("Enter the aiport number for which you want to find the nearest");;
                  int airport3=inputInt();
                  nearest(airportList.get(airport3));
                break;
                case 4:
                 addToAirportList();   
                break;
                case 0:
                    System.out.println("Exiting,Good bye");
                break;
                default:
                    throw new ParameterInvalidException("Out of range selection");
        }
        }//end of try
        catch(ParameterInvalidException ex){
        throw new ParameterInvalidException("Latitude range from 0 to 90.0");
    }
        }//end while
        
        
    }//end main method

}//end main class
