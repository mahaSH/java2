//Done by Maha M. Shawkat
package day09geometryfromfile;

import static java.awt.PageAttributes.MediaType.A;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

class InvalidDataException extends Exception {

    public InvalidDataException(String msg, Throwable cause) {
        super(msg, cause);
    }//End of DataInvalidException constructor

    public InvalidDataException(String msg) {
        super(msg);
    }
}//End of class 

abstract class GeoObj {

    private String Color;

    //Abstract methods
    abstract double getSurface()throws UnsupportedOperationException;

    abstract double getCircumPerimer()throws UnsupportedOperationException;

    abstract int getVerticesCount()throws UnsupportedOperationException;

    abstract int getEdgesCout()throws UnsupportedOperationException ;
    //methods
    public String getColor() {
        return Color;
    }

    public void setColor(String Color) throws InvalidDataException {
        if (!Color.matches("[A-Z a-z \\- -]*")) {
            throw new InvalidDataException("Error: invalid color patteren");
        }//end patteren checking
        if (Color.length() < 1) {
            throw new InvalidDataException("Error: invalid color entered");
        }//En of check and throw    
        this.Color = Color;
    }

    @Override
    public String toString() {
        return String.format("Shape color is", getColor());
    }

}//End of class GeoObj

/**
 * ***********************************************
 */
class Point extends GeoObj {

    @Override
    public String toString() {
        return String.format("Point;%s", getColor());
    }
   //abstract methods implementatin
   public double getSurface()throws UnsupportedOperationException{
       return(0);
}//End getSurface
   public  double getCircumPerimer()throws UnsupportedOperationException{
return(0);

   }//End getCircumPerimer
   public  int getVerticesCount()throws UnsupportedOperationException{
return 1;
   }//end getVerticesCount
  public int getEdgesCout()throws UnsupportedOperationException{
return 1;
}//end getEdgesCout

}//End of class point

/**
 * ************************************************
 */
class Rectangle extends GeoObj {

    private double length;
    private double width;

    //constructors,Setters and getters
    public Rectangle() {
    }

    public Rectangle(double length, double width) throws InvalidDataException {
        setLength(length);
        setWidth(width);
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) throws InvalidDataException {
        if (length < 0.0) {
            throw new InvalidDataException("Invalid rectangle length enteredrd");
        }//End checking length
        this.length = length;
    }

    public double getWidth() {

        return width;
    }

    public void setWidth(double width) throws InvalidDataException {
        if (width < 0.0) {
            throw new InvalidDataException("Invalid width entered");
        }//end checking length
    }

    @Override
    public String toString() {
        return String.format("Rectangle;%s;%f ;%f", getColor(), getLength(), getWidth());
    }
    //abstract methods implementatin
   public double getSurface()throws UnsupportedOperationException{
       return (this.length*this.width);
   
}//End getSurface
   public  double getCircumPerimer()throws UnsupportedOperationException{
return((this.length+this.width)*2);
   }//End getCircumPerimer
   public  int getVerticesCount()throws UnsupportedOperationException{
   return 4;
   }//endgetVerticesCount
  public int getEdgesCout()throws UnsupportedOperationException{
     return 4;
}//end getEdgesCout
}//End of class Rectangle

/**
 * ************************************************
 */
class Circle extends GeoObj {

    private double radius;
//constructors,Setters and getters

    public Circle() {
    }

    public Circle(double radius) throws InvalidDataException {
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws InvalidDataException {
        if (radius < 0.0) {
            throw new InvalidDataException("Radius should be less than zero");
        }//End of error checking
        this.radius = radius;
    }

    @Override
    public String toString() {
        return String.format("Circle;%s;%f", getColor(), getRadius());
    }
//abstract methods implementatin
   public double getSurface()throws UnsupportedOperationException{
       return this.radius*this.radius*Math.PI;
}//End getSurface
   public  double getCircumPerimer()throws UnsupportedOperationException{
       return 2*this.radius*Math.PI;
   }//End getCircumPerimer
   public  int getVerticesCount()throws UnsupportedOperationException{
       return 0;
   
   }//endgetVerticesCount
  public int getEdgesCout()throws UnsupportedOperationException{
      return 0;
}//end getEdgesCout
  
}//End of class Circle

/**
 * ************************************************
 */
class Sphere extends GeoObj {

    private double radius;
    //Constructers setters and getters

    public Sphere() {
    }

    public Sphere(double radius) throws InvalidDataException {
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) throws InvalidDataException {
        if (radius <= 0.0) {
            throw new InvalidDataException("Redius should be greater than zero");
        }//End checking block
        this.radius = radius;
    }

    @Override
    public String toString() {
        return String.format("sphere;%s;%f", getColor(), getRadius());
    }
//abstract methods implementatin
   public double getSurface()throws UnsupportedOperationException{
return(4*Math.PI*this.radius*this.radius);
   }//End getSurface
   public  double getCircumPerimer()throws UnsupportedOperationException{
  return(4*Math.PI*this.radius*this.radius*(this.radius/3));
   }//End getCircumPerimer
   public  int getVerticesCount()throws UnsupportedOperationException{
   return 0;
   }//endgetVerticesCount
  public int getEdgesCout()throws UnsupportedOperationException{
return 0;
  }//end getEdgesCout
}//End of class sphere

/**
 * **********************************************
 */
class Hexagon extends GeoObj {

    private double edge;

    //Constructors and methods
    public Hexagon() {
    }

    public Hexagon(double edge) throws InvalidDataException {
        setEdge(edge);
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) throws InvalidDataException {
        if (edge <= 0.0) {
            throw new InvalidDataException("Edge should be longer than zero");
        }//End checking block
        this.edge = edge;

    }

    @Override
    public String toString() {
        return String.format("Hexagon;%s;%f", getColor(), getEdge());
    }
//abstract methods implementatin
   public double getSurface()throws UnsupportedOperationException{
return 3*Math.sqrt(3)/2*this.edge*this.edge;
   }//End getSurface
   public  double getCircumPerimer()throws UnsupportedOperationException{
   return 6*this.edge;
   }//End getCircumPerimer
   public  int getVerticesCount()throws UnsupportedOperationException{
   return 6;
   }//endgetVerticesCount
  public int getEdgesCout()throws UnsupportedOperationException{
return 6;
  }//end getEdgesCout
}//End hexagon class

/**
 * **********************************************
 */
class Square extends Rectangle {

    private double edge;

//Constructors, setters and getters
    public Square() {
    }

    public Square(double edge) throws InvalidDataException {
        setEdge(edge);
    }

    public double getEdge() {
        return edge;
    }

    public void setEdge(double edge) throws InvalidDataException {
        if (edge <= 0.0) {
            throw new InvalidDataException("Edge should be greater than zero");

        }//end checking block
        this.edge = edge;
    }

    @Override
    public String toString() {
        return String.format("Square;%s;%f", getColor(), getEdge());
    }
}//End of class square

/**
 * ************************************************
 */

/**
 * ************************************************
 */
public class Day09GeometryFromFile {

    static ArrayList<GeoObj> geoList = new ArrayList<>();

    //Adding factory method
    static GeoObj createFromLine(String dataLine) throws InvalidDataException {
        GeoObj object1 = null;
        String[] data = dataLine.split(";");
        try {
            if (data.length < 2) {
                throw new InvalidDataException("Error:Invalid line entered");
            }//End if
            if (data[0].equals("Rectangle")) {
                Rectangle object = new Rectangle();
                object.setColor(data[1]);
                object.setWidth(Double.parseDouble(data[2]));
                object.setLength(Double.parseDouble(data[3]));
                object1 = object;

            }//End if rectangle
            else if (data[0].equals("Circle")) {
                Circle object = new Circle();
                object.setColor(data[1]);
                object.setRadius(Double.parseDouble(data[2]));
                object1 = object;
                return (object1);
            }//End if circle
            else if (data[0].equals("Point")) {
                Point object = new Point();
                object.setColor(data[1]);
                object1 = object;

            }//End if point
            else if (data[0].equals("Sphere")) {
                Sphere object = new Sphere();
                object.setColor(data[1]);
                object.setRadius(Double.parseDouble(data[2]));
                object1 = object;

            }//End if Sphere
            else if (data[0].equals("Square")) {
                Square object = new Square();
                object.setColor(data[1]);
                object.setEdge(Double.parseDouble(data[2]));
                object1 = object;

            }//aEnd if square
            else if (data[0].equals("Hexagon")) {
                Hexagon object = new Hexagon();
                object.setColor(data[1]);
                object.setEdge(Double.parseDouble(data[2]));
                object1 = object;

            }//End if Hexagon
            else {
                throw new InvalidDataException("Invalid data entered");
            }//End of else statement
            /* catch(ParseException ex){
   throw new InvalidDataException("Eror:Parsing failed,ex");
   }//End of catch method*/
            return (object1);
        }//end try block
        catch (NumberFormatException ex) {
            throw new InvalidDataException("Not valid parsed  string");
        }
    }//End factory method

//Static common mtheods
    /* */
    public static void main(String[] args) {
        try {
            Scanner inputFile = new Scanner(new File("objects.txt"));
            while (inputFile.hasNextLine()) {
                try {
                    String outLine = inputFile.nextLine();
                    GeoObj object = createFromLine(outLine);
                    geoList.add(object);
                } catch (InvalidDataException ex) {
                    System.out.println("invalid line input,canceled");
                }//end intenal catch
            }//End while loop
        }//End try
        catch (FileNotFoundException ex) {
            System.out.println("No file to be read,the program will exit");
        }//End catch Statement

        for (int i = 0; i < geoList.size(); i++) {
            System.out.println(geoList.get(i));
        }
    }//end of main method

}
