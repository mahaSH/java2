/*package day10carssorted;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Car implements Comparable<Car> {

    String makeModel;
    double engineSizeL;
    int prodYear;

    public Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public double getEngineSizeL() {
        return engineSizeL;
    }

    public int getProdYear() {
        return prodYear;
    }

    @Override
    public String toString() {
        return String.format("Make model is %s,engine size is %.2f production year is %d", makeModel, engineSizeL, prodYear);

    }

    public int compareTo(Car c1) {
        return (this.getMakeModel().compareTo(c1.getMakeModel()));
    }

}//end class car

class EngineSizeCompare implements Comparator<Car> {

    public int compare(Car c1, Car c2) {
        if (c1.getEngineSizeL() > c2.getEngineSizeL()) {
            return 1;
        }
        if (c1.getEngineSizeL() < c2.getEngineSizeL()) {
            return -1;
        } else {
            return 0;
        }
    }
}//End of class EngineSizeCompare

class ProductionYearCompare implements Comparator<Car> {

    public int compare(Car c1, Car c2) {
        if (c1.getProdYear() > c2.getProdYear()) {
            return 1;
        }
        if (c1.getProdYear() < c2.getProdYear()) {
            return -1;
        } else {
            return 0;
        }
    }//End compare method
}//End of ProductionYearCompare

class ProductionAndModelCompare implements Comparator<Car> {

    public int compare(Car c1, Car c2) {
        int ret=0;
        if (c1.getProdYear() == c2.getProdYear()) {
            ret=(c1.getMakeModel().compareTo(c2.getMakeModel()));
        }//end if year
        else {
            if (c1.getProdYear() > c2.getProdYear()) {
                ret= 1;
            }
            if (c1.getProdYear() < c2.getProdYear()) {
                ret= -1;
            }
        } //End else
return ret;
    }//end compare method
}//end ProductionAndEngineCompare

public class Day10CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();

    public static void main(String[] args) {
        Car c1 = new Car("honda", 4.8, 1977);
        parking.add(c1);
        Car c2 = new Car("toyota", 4, 1977);
        parking.add(c2);
        Car c3 = new Car("chevrolet", 3, 1967);
        parking.add(c3);
        Car c4 = new Car("kia", 1.6, 1987);
        parking.add(c4);
        Car c5 = new Car("opel", 2.6, 2003);
        parking.add(c5);

        //printing one by one
        for (int i = 0; i < parking.size(); i++) {
            System.out.println(parking.get(i));
        }
        //Sorting by name
        Collections.sort(parking);
        System.out.println("cars after sorting");
        for (int i = 0; i < parking.size(); i++) {
            System.out.println(parking.get(i));
        }//end for loop
        //Sorting by engine 
        EngineSizeCompare engine = new EngineSizeCompare();
        Collections.sort(parking, engine);
        System.out.println("Engine capacity sorting");
        for (int i = 0; i < parking.size(); i++) {
            System.out.println(parking.get(i));
        }//end loop
        //Sorting by year
        ProductionYearCompare year = new ProductionYearCompare();
        Collections.sort(parking, year);
        System.out.println("production year sort");
        for (int i = 0; i < parking.size(); i++) {
            System.out.println(parking.get(i));
        }//end loop
        ProductionAndModelCompare compare=new ProductionAndModelCompare();
        Collections.sort(parking, compare);
        System.out.println("year then mark year sort");
        for (int i = 0; i < parking.size(); i++) {
            System.out.println(parking.get(i));
        }//end loop
    }//end main
}//end main class*/
package day10carssorted;

import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;

class Car implements Comparable<Car> {

    String makeModel;
    double engineSizeL;
    int prodYear;

    public Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    @Override
    public String toString() {
        return "Car{" + "makeModel=" + makeModel + ", engineSizeL=" + engineSizeL
                + ", prodYear=" + prodYear;
    }

    @Override
    public int compareTo(Car otherCar) {
        return makeModel.compareTo(otherCar.makeModel);
    }


    static Comparator<Car> CompareByEngineSize = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            if (car1.engineSizeL < car2.engineSizeL) {
                return -1;
            }
            if (car1.engineSizeL > car2.engineSizeL) {
                return 1;
            } else {
                return 0;
            }
        }
    }; // use ; because it is a statement

    static Comparator<Car> CompareByProdYear = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.prodYear - car2.prodYear;
        }
    };

    static Comparator<Car> CompareByProdYearThenMakeModel = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            int result = car1.prodYear - car2.prodYear;
            if (result != 0) {
                return result;
            }
            return car1.makeModel.compareTo(car2.makeModel);
        }
    };

} // end class car

public class Day10CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();

    public static void main(String[] args) {
        parking.add(new Car("WW Fusca", 35.0, 1987));
        parking.add(new Car("Ford Model T", 15.5, 1955));
        parking.add(new Car("Citroen DS", 20.0, 1955));
        parking.add(new Car("Chevrolet Camaro", 25.5, 1967));
        parking.add(new Car("Ford Mustang GT", 30.0, 1987));

        System.out.println("Original list");
        for (Car c : parking) {
            System.out.println(c);
        }
        
        System.out.println("Cars sort by makeModel");
        Collections.sort(parking);
        for (Car c : parking) {
            System.out.println(c);
        }

        System.out.println("Cars sort by engineSizeL");
        Collections.sort(parking, Car.CompareByEngineSize);
        for (Car c : parking) {
            System.out.println(c);
        }

        System.out.println("Cars sort by prodYear");
        Collections.sort(parking, Car.CompareByProdYear);
        for (Car c : parking) {
            System.out.println(c);
        }
        
        System.out.println("Cars sort by prodYear then makeModel");
        Collections.sort(parking, Car.CompareByProdYearThenMakeModel);
        for (Car c : parking) {
            System.out.println(c);
        }    
    }
}

