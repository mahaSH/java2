package day10numberfrequency;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

class NumFreq {

    public NumFreq(int number, int frequency) {
        this.number = number;
        this.frequency = frequency;
    }

    int number;
    int frequency;

    public int getNumber() {
        return number;
    }

    public int getFrequency() {
        return frequency;
    }
    
}//end class

public class Day10NumberFrequency implements Comparable<NumFreq>
{

    static ArrayList<Integer> numbers = new ArrayList<>();
    static ArrayList<NumFreq> frequenc = new ArrayList<>();
    //static ArrayList sorted = new ArrayList<>();

    public static void main(String[] args) {
        //int counter = 0;
        try {
            Scanner inputFile = new Scanner(new File("numbers.txt"));
            while (inputFile.hasNextInt()) {
                numbers.add(inputFile.nextInt());
            }//End wile
        } /*for (int i = 0; i < numbers.size(); i++) {
                counter = 0;
                for (int j = 0; j < numbers.size(); j++) {
                    if (numbers.get(i) == numbers.get(j)) {
                      //sorted.add(counter);
                        counter++;   
                    }
                   
                        
                }//end for j
                frequency.add(counter);
                sorted.add(counter);
            }//end for i
            Collections.sort(sorted);
            System.out.print("Number | Occurances \n");
            System.out.print("-------+-----------");
            for (int i = 0; i < numbers.size(); i++) {
                for (int j = 0; j < numbers.size(); j++) {
                 if(sorted.get(i)==frequency.get(j)){
                     System.out.printf("%n %6d |%11d ",numbers.get(j),frequency.get(j)); 
                    
                 } //end if 
                   
                }
                 break;
            }
            
        }//End try*/ catch (IOException ex)  {
            System.out.println("error reading file,exiting");
            return;
        }
        for (int n : numbers) {
            NumFreq foundNf = null;
            for (NumFreq nf : frequenc) {
                if (nf.number == n) {
                    foundNf = nf;
                    break;
                }
            }
             if(foundNf==null){
            NumFreq newNf=new NumFreq(n,1);
            frequenc.add(newNf);
        }
             else{
                 foundNf.frequency++;
             }
        }
       
    /*}

    @Override
    public int compareTo(Integer o) {
        return (this.compareTo(o));*/
        System.out.println("FreqList:"+frequenc);
        Collections.sort(frequenc);
        System.out.print("Number | Occurrences \n" +
"-------+------------ ");
    }

    @Override
    public int compareTo(NumFreq arg0) {
       return (this.compareTo(arg0));
    }

    

}
