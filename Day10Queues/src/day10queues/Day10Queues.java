/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10queues;

import java.util.ArrayList;

class LIFO<T> {

    private ArrayList<T> storage = new ArrayList<>();

    public void push(T element) {
        storage.add(element);
    }

    public T pop() {
        if (storage.size() == 0) {
            throw new IndexOutOfBoundsException("popping out of range,list is empty");
        }
        int top = storage.size();
        storage.remove(top);
        return storage.get(top);

    }// throws IndexOutOfBoundsException on empty

    public T peek() {
        T top = storage.get(storage.size());
        return (top);
    }// returns top of the stack without taking it off

    public int size() {
        return (storage.size());
    }
    // also implement toString that prints all items in a single line, comma-separated
}

class FIFO<T> {

    private ArrayList<T> storage = new ArrayList<>();

    public void add(T obj){
        storage.add(obj);
    }

    public T remove(){
        if(storage.isEmpty()){
            throw new IndexOutOfBoundsException ("popping out of range,list is empty");
        }
     return(storage.remove(0));
    }
    public int size(){
     return (storage.size())   ;
    }
    // also implement toString that prints all items in a single line, comma-separated

    @Override
    public String toString() {
        return String.format("FIFO{" + "storage=" + storage + '}');
    }

   
}

public class Day10Queues {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FIFO<Integer> obj = new FIFO<Integer>();
        obj.add(12);
        obj.add(40);
        obj.add(56);
        obj.add(63);
        obj.remove();
        obj.remove();
        obj.remove();
        obj.remove();
        obj.remove();
        obj.size();
        System.out.println(obj);
        System.out.println("size="+obj.size());
    }

}
