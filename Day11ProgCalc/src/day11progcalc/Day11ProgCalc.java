    /*/*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
     /*package day11progcalc;

    import java.io.File;
    import java.io.FileNotFoundException;
    import java.util.ArrayList;
    import java.util.Scanner;

    class LIFOStack<T> {

        private ArrayList<T> storage = new ArrayList<>();

        public void push(T element) {
            storage.add(element);
        }

        public T pop() {
            if (storage.size() == 0) {
                throw new IndexOutOfBoundsException("popping out of range,list is empty");
            }
            int top = storage.size();
            storage.remove(top);
            return storage.get(top);

        }// throws IndexOutOfBoundsException on empty

        public T peek() {
            T top = storage.get(storage.size());
            return (top);
        }// returns top of the stack without taking it off

        public int size() {
            return (storage.size());
        }
        // also implement toString that prints all items in a single line, comma-separated

    }

    public class Day11ProgCalc {

        static Scanner input = new Scanner(System.in);
        static boolean check;

        public static void main(String[] args) {
            LIFOStack<Double> stack = new LIFOStack<>();
            try {
                Scanner inputFile = new Scanner(new File("program.txt"));
                while (inputFile.hasNextLine()) {
                    String str = input.nextLine();
                    check = checkLine(str);
                    if (check) {
                        Double num = Double.parseDouble(str);
                        stack.push(num);
                        continue;
                    } else if (str.substring(0, 1).equals("?:")) {
                        System.out.println(str.substring(2, str.length()));
                        System.out.println("Enter two float numbers ");
                        stack.push(input.nextDouble());
                        input.nextDouble();
                        continue;

                    }
                    else{
                    switch (str) {
                        case "+": {
                            double num1 = stack.pop();
                            double num2 = stack.pop();
                            stack.push(num1 + num2);
                            continue;
                        }
                        case "-": {
                            double num3 = stack.pop();
                            double num4 = stack.pop();
                            stack.push(num3 + num4);
                            continue;
                        }
                        case "*": {
                            double num5 = stack.pop();
                            double num6 = stack.pop();
                            stack.push(num5 * num6);
                            continue;
                        }
                        case "/": {
                            double num7 = stack.pop();
                            double num8 = stack.pop();
                            stack.push(num7 / num8);
                            continue;
                        }
                        case "%": {
                            double num9 = stack.pop();
                            double num0 = stack.pop();
                            stack.push(num9 % num0);
                            continue;
                        }
                        case "=": {
                            double peek = stack.peek();
                            System.out.printf("the value on top of the stack %.3f", peek);
                            continue;
                        }
                        case "pop": {
                            double pop = stack.pop();
                            continue;}
                            default:
                            {
                            System.out.println("warning: invalid line, skipping");
                        }

                        }//end switch
                    }
                    }//end while


                }catch (FileNotFoundException ex) {
                System.out.println("file not found");
            }//end catch

            }



        public static boolean checkLine(String line) {
            try {
                Double.parseDouble(line);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
    }//end class*/
    package day11progcalc;

    import java.io.File;
    import java.io.FilenameFilter;
    import java.io.IOException;
    import java.util.ArrayList;
    import java.util.Scanner;

    class LIFOStack<T> {

        private ArrayList<T> storage = new ArrayList<>();

        void push(T item) {
            storage.add(0, item);
        }

        T pop() { // throw IndexOutOfBoundsException on empty
            if (storage.isEmpty()) {
                throw new IndexOutOfBoundsException("Stack empty");
            }
            return storage.remove(0);
        }

        T peek() { // returns top of the stack without taking it off
            if (storage.isEmpty()) {
                throw new IndexOutOfBoundsException("Stack empty");
            }
            return storage.get(0);
        }

        int size() {
            return storage.size();
        }

        @Override
        public String toString() {
            return "LIFOStack" + storage.toString();
        }

    }

    public class Day11ProgCalc {

        static public void implementFile(String str) {
            try (Scanner fileInput = new Scanner(new File(str))) {
                while (fileInput.hasNextLine()) {
                    String line = fileInput.nextLine();
                    // is it a floating point number? yes - push it
                    try {
                        double numVal = Double.parseDouble(line);
                        stack.push(numVal);
                        continue;
                    } catch (NumberFormatException ex) {
                        // do nothing, just continue below
                    }
                    // 
                    switch (line) {
                        case "+": {
                            double v1 = stack.pop();
                            double v2 = stack.pop();
                            double result = v1 + v2;
                            stack.push(result);
                            continue;
                        }
                        case "-": {
                            double v1 = stack.pop();
                            double v2 = stack.pop();
                            double result = v1 - v2;
                            stack.push(result);
                            continue;
                        }
                        case "*": {
                            double v1 = stack.pop();
                            double v2 = stack.pop();
                            double result = v1 * v2;
                            stack.push(result);
                            continue;
                        }
                        case "/": {
                            double v1 = stack.pop();
                            double v2 = stack.pop();
                            double result = v2 / v1;
                            stack.push(result);
                            continue;
                        }
                        case "%": {
                            double v1 = stack.pop();
                            double v2 = stack.pop();
                            double result = v1 % v2;
                            stack.push(result);
                            continue;
                        }
                        case "=": {
                            double value = stack.peek();
                            System.out.printf("Top of stack: %.4f\n", value);
                            continue;
                        }
                        case "pop": {
                            stack.pop();
                            continue;
                        }
                        default:
                            // if (line.charAt(0) == '?') { }
                            if (line.matches("\\?:.*")) {
                                String message = line.split(":", 2)[1];
                                System.out.print(message);
                                double value = input.nextDouble();
                                stack.push(value);
                            } else {
                                System.out.println("warning: invalid line, skipping");
                            }
                    } //end of switch
                    //
                }
            } catch (IOException ex) {
                System.out.println("Error reading file: " + ex.getMessage());
            }
        }
        static LIFOStack<Double> stack = new LIFOStack<>();
        static Scanner input = new Scanner(System.in);

        public static void main(String[] args) {

            File directoryPath = new File("C:\\Users\\TJ\\Documents\\java2\\Day11ProgCalc\\");

            System.out.println("\n--------------------------------------");
            File[] files = directoryPath.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".txt");
                }
            });
            System.out.print("Please choose program to execute: \n");
            int i = 1;
            for (File file : files) {
                System.out.println(i++ + "-" + file.getName());

            }
            System.out.print("enter your choise: ");
            int choise = input.nextInt();

            System.out.printf("... (program executes here from  %s  file) ...%n", files[choise - 1].getName());
            implementFile(files[choise - 1].getName());
        }//end main

    }
