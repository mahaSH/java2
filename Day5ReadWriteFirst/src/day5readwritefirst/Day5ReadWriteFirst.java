
package day5readwritefirst;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Day5ReadWriteFirst {



    /*public static void main(String[] args) throws IOException {
       Scanner input=new Scanner(System.in);
       FileWriter fileWriter = new FileWriter("file3.txt");
       PrintWriter printWriter = new PrintWriter(fileWriter);
       String line;
       File file=new File("C:\\Users\\6155202\\Documents\\java2\\Day5ReadWriteFirst\\file3.txt");
       Scanner inputFile=new Scanner(file);
       
        System.out.println("Enter a line of text");
        line=input.nextLine();
        for(int i=0;i<3;++i){
            printWriter.println(line);
            
        }
    printWriter.close();
    while (inputFile.hasNextLine()) 
      System.out.println(inputFile.nextLine()); 
}*/
    static Scanner input=new Scanner(System.in);
public static void main(String[] args){
     System.out.println("Enter a line of text:");
     String userLine=input.nextLine();
     try(PrintWriter pw=new PrintWriter(new File("file.txt"))){
         for (int i = 0; i < 3; i++) {
             pw.println(userLine);
             
         }
     }catch (IOException ex){
         System.out.println("Error readin gfile:"+ex.getMessage());
     }
     //
     
     System.out.println("file contents:");
     try(Scanner fileInput=new Scanner (new File("file.txt"))){
         while(fileInput.hasNextLine()){
             String outline=fileInput.nextLine();
             System.out.println(outline);
         }
     }
     catch(IOException ex){
         System.out.println("Error reading file:"+ex.getMessage());
     }
         }
     }
       