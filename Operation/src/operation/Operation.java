/*
 Ask user to choose an option from menu:
+1. Add
+2. Subtract
+3. Multiply
+4. Divide
+
+If user enters an invalid choice, other than 1-4, display error message and exit.
+
+Otherwise ask user for 2 floating point values,
+perform the requested computation and display the result.
+
+BONUS:
+
+Add one more option to the menu.
+0. Exit
+
+And add a loop around the entire program so that
+the menu is displayed repeatedly until user asks to exit.
+
+Suggestion: instead of do-while loop use
+an infinite loop and a break/return.
 */
package operation;

/**
 *
 * @author Maha M. Shawkat
 */
import java.util.*;

public class Operation {

    
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int choice;
        float number1;
        float number2;
        float result=0;
        for (int i = 0; ; i++) {
        System.out.println("Enter your choice:\n0.Exit \n1.Add\n" +"2.Subtract\n" +"3.Multiply\n" +"4.Divide");
        choice=input.nextInt();
        if(choice!=1&&choice!=2&&choice!=3&&choice!=4&&choice!=0){
        System.out.println("Error:out of range!!");
        return;
    }
        else if(choice==0){
            return;
            }
        else{
        System.out.println("Enter two float numbers:\n");
        number1=input.nextFloat();
        number2=input.nextFloat();
        input.nextLine();
        switch (choice) {
            case 1:
            result=number1+number2;
            case 2:
            result=number1-number2;
            case 3:
            result=number1*number2;
            case 4:
            result=number1/number2;
           }
        System.out.printf("The result is %f",result);
        
    }
    }
    } 
}
