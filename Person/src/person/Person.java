
package person;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Person {

    private String name;
    private int age;
     public Person(){
         
     }
      public Person(String name1,int age1){
         this.name=name1;
         this.age=age1;
     }
      public void print(){
          System.out.printf("%n%s is %d y/o",name,age);
      }
      static ArrayList<Person> peopleList = new ArrayList<>();
     
    public static void main(String[] args) {
       
        /*Person person1=new Person("jerry",33);
        peopleList.add(person1);
        Person person2=new Person("Maria",22);
        peopleList.add(person2);
        Person person3=new Person("jerry",54);
        peopleList.add(person3);
        for (int i = 0; i < 3; i++) {
          peopleList.get(i).print();
        }
         //Find the youngest and oldest person and display their name and age.
       Person  youngest=peopleList.get(0);
       for (int i = 0; i < 3; i++) {
           if (peopleList.get(i).age<youngest.age){
               youngest=peopleList.get(i);  
           }
           
        }
        System.out.printf("%n The youngest person is %s with %d years old.",youngest.name,youngest.age);
        //
        Person  oldest=peopleList.get(0);
       for (int i = 0; i < 3; i++) {
           if (peopleList.get(i).age>oldest.age){
               oldest=peopleList.get(i);  
           }
           
        }
        System.out.printf("%n The oldest person is %s with %d years old.",oldest.name,oldest.age);
        */
       /*PART B
        Instead of instantiating 3 objects in the main method read in the contents of the file, parse it to create objects with provided names and ages.
        Suggestion: use String.split() to separate data, then Integer.parseInt() to parse the age.
        The print out each person's info on a separate line.
         */
       try(Scanner inputFile=new Scanner(new File("people.txt"))){
           while(inputFile.hasNextLine()){ 
               
               String outLine=inputFile.nextLine();
               String [] parts=outLine.split(";");
              Person person=new Person(parts[0],Integer.parseInt(parts[1]));
               peopleList.add(person);
           }  
           for (int i = 0; i <  peopleList.size(); i++) {
               peopleList.get(i).print();
               
           }

       }       catch (FileNotFoundException ex) {
           System.out.println("Error reading file:"+ex.getMessage());
        }
       
}
}

    