//Done By maha M. Shawkat
package quiz2travel;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

 class Trip{
     

String travellerName, cityToVisit;
Date departureDate, returnDate;
//Constructor
Trip(String travellerName1 ,String cityToVisit1 ,Date departureDate1,Date returnDate1 ){
this.travellerName=travellerName1;
this.cityToVisit=cityToVisit1;
this.departureDate=departureDate1;
this.returnDate=returnDate1;
}
//type method
public void print() {
    System.out.printf("% travels to %s on %s and returns in %s ",travellerName,cityToVisit,departureDate,returnDate);
}

 }
 
public class Quiz2Travel {
static ArrayList<Trip> travelList = new ArrayList<>();
static Scanner input = new Scanner(System.in);

static void readDataFromFile() {
try(Scanner inputFile=new Scanner(new File("travels.txt"))){
    while(inputFile.hasNextLine()){
        try{
     String outLine=inputFile.nextLine();
     String [] parts=outLine.split(";");
     if(parts.length !=4){
      throw new InvalidParameterException("Error: invalid number of fields in line, skipping: " + outLine);  
    }//End of throwing
    //First part entering begins
     String travellerName=parts[0];
     if(travellerName.length()<1||travellerName.length()>50){
      throw new InvalidParameterException("Bad name entered,will escape it"+outLine)  ;
     }//End of first part
     //Second part entering begins
     String cityToVisit=parts[1];
     if(cityToVisit.length()<1||cityToVisit.length()>50){
      throw new InvalidParameterException("Bad city name entered,will escape it"+outLine)  ;
     }//End of second part
     //Third part entering begins
     String date1=parts[2];
     Date departureDate=new SimpleDateFormat("dd/MM/yyyy").parse(date1);
     //end of third part
     //Fourth part enterin begins
    String date2=parts[2];
     Date returnDate=new SimpleDateFormat("dd/MM/yyyy").parse(date2);
     //End of fourth part
     //comparing
     if(returnDate.before(departureDate)){
     throw new InvalidParameterException("Departure date must be before return date"+outLine)  ;    
     }
     Trip trip=new Trip(travellerName,cityToVisit,departureDate,returnDate);
     travelList.add(trip);
        }//end of try2
        
      catch(InvalidParameterException |ParseException ex) {
          System.out.println("Error parsing value: " + ex.getMessage());
      }//End catch statement
    }//End of try1
    
}
catch (FileNotFoundException ex) {
           System.out.println("Error reading file:"+ex.getMessage());
        }
}

    
    public static void main(String[] args) {
      readDataFromFile();
              int choise=0;
       
      while(true){
          try{
          System.out.println("Please choose an action:\n" +
"1. Display list of all travel plans using print()\n" +
"2. Add new Trip to the list from user input\n" +
"3. Find and display all trips that have not departed yet but will in the future\n" +
"4. Find and display the trip of the longest duration [(*) hard].\n" +
"0. Exit.");
      choise=input.nextInt();
      input.nextInt();
      switch(choise){
          case 1:
              for (int i = 0; i < travelList.size(); i++) {
              travelList.get(i).print();
              }
          break;
          case 2 :
              System.out.println("Enter the new trip travelle Name");
              String travellerName=input.nextLine();
              if(travellerName.length()<1||travellerName.length()>50){
              throw new InvalidParameterException("Bad name entered,will escape it")  ;
              }
              System.out.println("Enter the City to be visited");
              String cityToVisit=input.nextLine();
              if(cityToVisit.length()<1||cityToVisit.length()>50){
              throw new InvalidParameterException("Bad city name entered,will escape it")  ;
              }//En
              System.out.println("Enter the Departure date");
              Date departure=new SimpleDateFormat("dd/MM/yyyy").parse(input.nextLine());
              System.out.println("Enter the return date date");
              Date returnDate=new SimpleDateFormat("dd/MM/yyyy").parse(input.nextLine());
              //comparing
              if(returnDate.before(departure)){
              throw new InvalidParameterException("Departure date must be before return date")  ;    
                  }
              //end case 2 
          break;
          case 3 :for (int i = 0; i <  travelList.size(); i++) {
                if ((travelList.get(i).departureDate).after(Calendar.getInstance().getTime())){
                    travelList.get(i).print();
                }  
              }
          break;
          case 4:
              Trip longestDuration=travelList.get(0);
              for (int i = 0; i <  travelList.size(); i++){
              if( Math.abs(travelList.get(i).returnDate.getTime()- travelList.get(i).departureDate.getTime())> Math.abs(longestDuration.returnDate.getTime()- longestDuration.departureDate.getTime())){
                longestDuration=travelList.get(i);
              }
              longestDuration.print();
              }
           break;   
          case 0:
             break;
          default:
             throw new IllegalArgumentException("GPA value invalid: " + choise);
      }//End switch statement
      
          }//End of try 
          catch(ParseException ex) {
          System.out.println("Error parsing value: " + ex.getMessage());
}  
      }//end while loop
      
   
    
    }//End of main method
}
   
