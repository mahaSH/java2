/*
 Done by Maha Mahmood
 */
package quiz3furniture;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

class DataInvalidException extends RuntimeException {

    public DataInvalidException(String msg) {
        super(msg);
    }

    public DataInvalidException(String msg, Throwable cause) {
        super(msg, cause);
    }

}

interface Marshallable {

    String toDataString();
}

abstract class HomeItem implements Marshallable {

    static int TotalCreated;
    String description; // 2-50 characters except semicolon ; ` @ &
    double price; // 0-999999 floating point, display with 2 points precision

    public HomeItem(String description, double price) throws DataInvalidException {
        setDescription(description);
        setPrice(price);
    }

    static public int getTotalCreated() {
        return TotalCreated;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public void setDescription(String description) throws DataInvalidException {
        if (description.length() < 2 || description.length() > 50 || description.contains(";") || description.contains("`") || description.contains("@ ") || description.contains(" &")) {
            throw new DataInvalidException("Invalid description:it should be 2-50 characters except semicolon ; ` @ &");
        }
        this.description = description;
    }

    public void setPrice(double price) throws DataInvalidException {
        if (price < 0 || price > 999999) {
            throw new DataInvalidException("Invalid price:should be 0-999999 floating point ");
        }
        this.price = price;
    }

    abstract int getSerialNo();

    static Comparator<HomeItem> CompareByPrice=new Comparator<HomeItem>(){
      @Override
        public int compare(HomeItem item1, HomeItem item2) {
            if( item1.price>item2.price)
                return 1;
            else if ( item1.price<item2.price)
                return 1;
            else return 0;
        }
    };
    static Comparator<HomeItem> ComparePriceThenDescription=new Comparator<HomeItem>(){
      @Override
        public int compare(HomeItem item1, HomeItem item2) {
            if( item1.price>item2.price)
                return 1;
            else if ( item1.price<item2.price)
                return 1;
            else {
                if( item1.description.length()>item2.description.length())
                return 1;
                else if( item1.description.length()<item2.description.length())
                return 1;
                else return 0;
                    
            }
        }
    };
    
    
}

class FurnitureItem extends HomeItem {

    @Override
    public int getSerialNo() {
     return ();  
    }

    @Override
    public String toDataString() {
        String priceS=Double.toString(price);
        String line="Furniture;"+priceS+";"+description+";"+itemType;
        return(line);
    }
    enum HomeItemType {
        Desk, Chair, Table, Sofa, Bed
    }
    HomeItemType itemType;

    public FurnitureItem(double price, String description,HomeItemType itemType) throws DataInvalidException {
        super(description, price);
        setItemType(itemType);
    }

    public HomeItemType getItemType() {
        return itemType;
    }

    public void setItemType(HomeItemType itemType) {
        this.itemType = itemType;
    }

    @Override
    public String toString() {
        return String.format("Furniture section:the price is %.2f,%s,it is a ,%s",price,description,itemType);
    }
    
    
    

    
}//furn
class ElectronicsItem extends HomeItem {
  String model; // 2-50 characters except semicolon ;

    public ElectronicsItem(double price, String description,String model ) throws DataInvalidException {
        super(description, price);
        setModel (model);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model.length()<2||model.length()>50||model.contains(";")){
            throw new DataInvalidException("Error: mode shoul be  2-50 characters except semicolon ;"); 
        }
        this.model = model;
    }

    @Override
    public String toString() {
        return String.format("From electronics section:the price is %.2f ,%s ,maodel %s",price,description,model);
    }

    @Override
    int getSerialNo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toDataString() {
        String priceS=Double.toString(price);
        String line="Electronics;"+priceS+";"+description+";"+model;
        return(line);
    }

  
}


public class Quiz3Furniture {
   static ArrayList<HomeItem> homeItemsList = new ArrayList<>();
   
    static HomeItem createFromDataLine(String line) throws DataInvalidException {
        String[] data = line.split(";");
        switch (data[0]) {
            case "Furniture": {
                if (data.length != 4) {
                    throw new DataInvalidException("Error: invalid  line: " + line);
                }
                double price = Double.parseDouble(data[1]);
                String discription = data[2];
                
                return new FurnitureItem(price, discription, Enum.Parse(data[3]));
               
            }//case furn
//case furn
            case "ElectronicsItem":
                if (data.length != 4) {
                    throw new DataInvalidException("Error: invalid  line: " + line);
                }
                double price = Double.parseDouble(data[1]);
                String discription = data[2];
                String model =  data[3];
                return new  ElectronicsItem(price, discription,model);
                
            default:
                throw new DataInvalidException ("Not valid line");
                
        }//switch
    }// factory method
    
    static void readDataFromFile(){
       try { 
           Scanner inputFile=new Scanner (new File("household.txt" ));
           
               while(inputFile.hasNextLine()){
             HomeItem item=createFromDataLine(inputFile.nextLine()) ;
             homeItemsList.add(item);
           }
           
           }//try2
           
       catch (FileNotFoundException  ex) {
           throw new DataInvalidException("Error reading file:no files exist! ;");
       }
        }//readDataFromFile 

    
    public static void main(String[] args) {
        readDataFromFile();
        //1
        for(HomeItem item: homeItemsList){
            System.out.println(item);
        }
        //2
                System.out.println("Total created items are"+homeItemsList.get(0).getTotalCreated());
        //3
        Collections.sort(homeItemsList,HomeItem.CompareByPrice);
        for(HomeItem item: homeItemsList){
            System.out.println(item);
        }
        //4
        Collections.sort(homeItemsList,HomeItem.ComparePriceThenDescription);
        for(HomeItem item: homeItemsList){
            System.out.println(item);
        }
        //
        //
          Collections.sort(homeItemsList,HomeItem.CompareByPrice);
        for(HomeItem item: homeItemsList){
            System.out.println(item);
        }
    }

}
