/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randomstore;

import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author TJ
 */


 public  class RandomStore{
     
     
     public int nextInt(int minIncl, int maxExcl) {
         int number=(int)(Math.random()*((maxExcl-minIncl)+1))+minIncl;
         return number;
         
      
     }

        
 
    public void printHistory(){
         System.out.printf("values ");
         for (int i = 0; i < intHistory.size(); i++) {
             System.out.printf("%s %d" ,i==0? " ":";",intHistory.get(i));
         }
         
     }
     
  
    static ArrayList<Integer> intHistory = new ArrayList<>();  
    public static void main(String[] args) {
       RandomStore rs = new RandomStore();
	int v1 = rs.nextInt(1, 10);
	int v2 = rs.nextInt(-100, -10);
	int v3 = rs.nextInt(-20,21);
	System.out.printf("Values %d, %d, %d\n", v1, v2, v3);
	rs.printHistory();
    }
 }